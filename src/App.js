import logo from "./logo.svg";
import "./App.css";
import CenteredCard from "./components/CenteredCard";

function App() {
  return (
    <div className="App">
      <div className="App-header">
        <CenteredCard name="test">
          <h2>Miqueias</h2>
          <p>
            Iniciando <strong>Q2</strong>
          </p>
          <p>
            Tentando aprender <strong>React</strong>
          </p>
        </CenteredCard>

        <CenteredCard name="test">
          <div className="quadro-maior">
            <h2>Carvalho</h2>
            <p>
              Iniciando <strong>Sprint 1</strong>
            </p>
            <p>Sem problemas no momento</p>
          </div>
        </CenteredCard>

        <CenteredCard name="test">
          <h2>dos Santos</h2>
          <p>Não tem criatividade</p>
          <p>Mas tá tentando</p>
        </CenteredCard>
      </div>
    </div>
  );
}

export default App;
